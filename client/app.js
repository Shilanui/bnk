'use strict';
    var videoApp = angular.module('videoApp', []);
    videoApp.controller('VideoController',['$scope','$window','$sce','$http', function($scope, $window,$sce,$http){
        $scope.data_video = [];
        $scope.data_array = [];
        $scope.length = 0;
        $scope.name = "";
        $scope.description = "";
        var url = "http://localhost:3000/bnk";
            
        $scope.init = function () {
            $scope.get_json();
        };

        $scope.get_json = function () {
            $http({
                url: url,
                method: 'GET',
                dataType: 'jsonp'
                
                }).then(function (response){
                    console.log("length = "+JSON.stringify(response.data.items.length));
                    $scope.length = response.data.items.length;
                    $scope.data_video = response.data;

                    for(var i=0;i<$scope.length;i++) {
                        $scope.data = "https://www.youtube.com/embed/"+response.data.items[i].id.videoId;
                        $scope.name = response.data.items[i].snippet.title;
                        $scope.description = response.data.items[i].snippet.description;
                        $scope.videoSource = $sce.trustAsResourceUrl($scope.data);
                        $scope.data_array.push($scope.videoSource);
                        // $scope.data_array.push({
                        //     'video': $scope.videoSource,
                        //     'name': $scope.name,
                        //     'description': $scope.description
                        // });
                    }
                },function (error){
                    console.log("error = "+error);
                });
        };
    }]);
    
var express = require('express');
var http = require('http');
var app = express();
var request = require("request");
var url = require('url');
var fs = require('fs');
var url_video = "https://s3-ap-southeast-1.amazonaws.com/ysetter/media/video-search.json";
var server;

app.get("/bnk", (req, res) => {
    request({
        url: url_video,
        json: true
    }, function (error, response, body) {
        if (!error && response.statusCode === 200) {
            res.send(JSON.stringify(body));
        }
    })

    
});

app.all("*", function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Cache-Control, Pragma, Origin, Authorization, Content-Type, X-Requested-With");
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST");
    return next();
});

app.listen(3000, () => {
    console.log("Server listening on port 3000!");
});
